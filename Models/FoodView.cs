﻿using System.ComponentModel.DataAnnotations;

namespace RealMenu.Models
{
    public class FoodView
    {
        public int ID { get; set; }
        
        [Required(ErrorMessage = "FoodID couldn't be 0")]
        public int FoodId { get; set; }
        
        public int ViewCount { get; set; }
    }
}
