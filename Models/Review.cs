﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace RealMenu.Models
{
    public class Review
    {
        [JsonIgnore]
        public int Id { get; set; }

        [Required(ErrorMessage = "Review Tittle couldn't be empty"), StringLength(64)]
        public string Tittle { get; set; }

        [Required(ErrorMessage = "Review text couldn't be empty"), StringLength(1000)]
        public string Description { get; set; }

        [Required(ErrorMessage = "Not rated")]
        public float Rating { get; set; }

        public string ImageUrl { get; set; }

        public DateTime Time { get; set; } = DateTime.Now;

        public int UserId { get; set; }

        [Required(ErrorMessage = "Food not defined")]
        public int FoodId { get; set; }

        [NotMapped]
        public IFormFile Image { get; set; }
    }
}
