﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace RealMenu.Models
{
    public class User
    {
        [JsonIgnore]
        public int Id { get; set; }

        [Required, StringLength(15)]
        public string Name { get; set; }

        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Email in not valid")]
        [Required(ErrorMessage = "Login must be not null")]
        public string Email { get; set; }

        [JsonIgnore]
        [Required(ErrorMessage ="Password must be not null")]
        public string Password { get; set; }

        public string ImageUrl { get; set; }
        [JsonIgnore]
        public UserRole Role { get; set; }

        public ICollection<Restaurant> Restaurants { get; set; }

        [JsonIgnore]
        public string RefreshToken { get; set; }
    }
}
