﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace RealMenu.Models
{
    public class Food
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { set; get; }
        public string AssetKey { get; set; }
        public int CategoryId { get; set; }
        public string Ingredients { set; get; }
        public string ImageUrl { get; set; }
        public byte Allergens { set; get; }
        public float MaxSize { set; get; }
        public float MinSize { set; get; }
        public string Price { set; get; }
        public byte Spicy { set; get; }
        public byte Vegetarian { set; get; }
        public int Weight { set; get; }
        public FoodModelBid modelBid { get; set; }
        
        [JsonIgnore]
        [NotMapped]
        public IFormFile Image { get; set; }
    }
}
