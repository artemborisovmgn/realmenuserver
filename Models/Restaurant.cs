﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace RealMenu.Models
{
    public class Restaurant
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Status { get; set; }

        public string Description { get; set; }
        
        public string ImageUrl { get; set; }

        public ICollection<Category> Categories { get; set; }

        public string BundleFileUrl { get; set; }

        [JsonIgnore]
        [NotMapped]
        public IFormFile BundleFile { get; set; }
    }
}
