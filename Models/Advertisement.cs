﻿using System.ComponentModel.DataAnnotations;

namespace RealMenu.Models
{
    public class Advertisement
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Tittle couldn't be empty"), StringLength(100)]
        public string AdvertisementType { get; set; }
        
        [Required(ErrorMessage = "Tittle couldn't be empty"), StringLength(100)]
        public string UserGroupType { get; set; }

        [Required(ErrorMessage = "RestaurantId couldn't be 0")]
        public int RestaurantId { get; set; }
    }
}