﻿using Microsoft.AspNetCore.Identity;

namespace RealMenu.Models
{
    public class UserRole : IdentityRole
    {
            public UserRole() : base() { }

            public UserRole(string name)
                : base(name)
            { }
    }
}
