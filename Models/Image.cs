﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace RealMenu.Models
{
    public class Image
    {
        [JsonIgnore]
        public int Id { get; set; }

        [Required(ErrorMessage = "Tittle couldn't be empty"), StringLength(1000)]
        public string Tittle { get; set; }

        [Required(ErrorMessage = "Image byte array empty")]
        public byte[] ImageData { get; set; }

        [Required(ErrorMessage ="Dont find destination of image")]
        public int SourceId { get; set; }
        
        [NotMapped]
        public IFormFile ImageFile { get; set; }
    }
}
