﻿using System.ComponentModel.DataAnnotations;

namespace RealMenu.Models
{
    public class CategoryView
    {
        public int ID { get; set; }
        
        [Required(ErrorMessage = "CategoryID couldn't be 0")]
        public int CategoryID { get; set; }
        
        public int ViewCount { get; set; }
    }
}
