﻿using System.ComponentModel.DataAnnotations;

namespace RealMenu.Models
{
    public class Platform
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "PlatformType couldn't be empty"), StringLength(30)]
        public string PlatformType { get; set; }

        [Required(ErrorMessage = "Language couldn't be empty"), StringLength(30)]
        public string Language { get; set; }

        [Required(ErrorMessage = "RestaurentID couldn't be 0")]
        public int RestaurantId { get; set; }
    }
}
