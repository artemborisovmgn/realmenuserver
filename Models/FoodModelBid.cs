﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace RealMenu.Models
{
    public class FoodModelBid
    {
        [JsonIgnore]
        public int Id { get; set; }
        public int Number { get; set; }
        public string Status { get; set; }
        public DateTime Time { get; set; }

        public string MediaFilePath { get; set; }

        [JsonIgnore]
        [NotMapped]
        public IFormFile MediaFile { get; set; }
    }
}
