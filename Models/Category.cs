﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace RealMenu.Models
{
    public class Category
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string ImageUrl  { get; set; }

        public int RestaurantId { get; set; }

        public ICollection<Food> Foods { get; set; }

        public int parentID { get; set; }

        [NotMapped]
        public ICollection<Category> childCategories { get; set; }

        public int CategoryType { get; set; }

        [JsonIgnore]
        [NotMapped]
        public IFormFile Image { get; set; }
    }
}
