﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace RealMenu.Models
{
    public class Statistics
    {
        public int ID { get; set; }

        [NotMapped]
        public ICollection<Advertisement> Advertisements { get; set; }

        [NotMapped]
        public ICollection<Platform> Platforms { get; set; }

        [NotMapped]
        public ICollection<FoodView> FoodViews { get; set; }

        [NotMapped]
        public ICollection<CategoryView> CategoryViews { get; set; }
    }
}
