﻿using Microsoft.AspNetCore.Http;
using Azure.Storage.Blobs;
using System.IO;
using System.Threading.Tasks;

namespace RealMenu.Helpers
{
    public static class FileHelper
    {
        public static async Task<string> UploadImage(IFormFile file) 
        {
            string connectionString = @"DefaultEndpointsProtocol=https;AccountName=realmenustorageaccount;AccountKey=tk5q+XAZwRhvdSmKSWVeX3POK44CciIgV/Hro/xajiBsKCg0YskP0KWpERVQcvdOwQHjgc34dtE9+AStRZr16w==;EndpointSuffix=core.windows.net";
            string containerName = "realmenuimage";

            BlobContainerClient blobContainerClient = new BlobContainerClient(connectionString, containerName);
            BlobClient blobClient = blobContainerClient.GetBlobClient(file.FileName);
            var memstream = new MemoryStream();
            await file.CopyToAsync(memstream);
            memstream.Position = 0;
            await blobClient.UploadAsync(memstream);
            return blobClient.Uri.AbsoluteUri;
        }
        public static async Task<string> UploadFile(IFormFile file)
        {
            string connectionString = @"DefaultEndpointsProtocol=https;AccountName=realmenustorageaccount;AccountKey=tk5q+XAZwRhvdSmKSWVeX3POK44CciIgV/Hro/xajiBsKCg0YskP0KWpERVQcvdOwQHjgc34dtE9+AStRZr16w==;EndpointSuffix=core.windows.net";
            string containerName = "usersfile";

            BlobContainerClient blobContainerClient = new BlobContainerClient(connectionString, containerName);
            BlobClient blobClient = blobContainerClient.GetBlobClient(file.FileName);
            var memstream = new MemoryStream();
            await file.CopyToAsync(memstream);
            memstream.Position = 0;
            await blobClient.UploadAsync(memstream);
            return blobClient.Uri.AbsoluteUri;
        }

    }
}
