﻿using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using RealMenu.Data;
using RealMenu.Interfaces;
using RealMenu.Models;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace RealMenu.Helpers
{
    public class JwtAuthenticationManager : IJwtAuthenticationManager
    {
        private ApiDBContext _dBContext;

        private readonly string _key;

        public JwtAuthenticationManager(ApiDBContext dBContext, string key)
        {
            _dBContext = dBContext;
            _key = key;
        }

        public async Task<Token> AuthenticateAsync(string email, string password)
        {
            var user = await _dBContext.Users.Where(e => e.Email.Equals(email) && e.Password.Equals(password))
                .SingleAsync();

            if (ReferenceEquals(user, null))
                return null;

            //if (!_dBContext.Users.Any(e => e.Email.Equals(email) && e.Password.Equals(password)))
            //{
            //    return null;
            //}

            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenKey = Encoding.UTF8.GetBytes(_key);
            var tokenDescription = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, email),
                }),
                Expires = DateTime.UtcNow.AddHours(1),
                SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(tokenKey),
                    SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescription);
            var AccessToken = tokenHandler.WriteToken(token);

            var refreshToken = await GenerateRefreshToken();
            user.RefreshToken = refreshToken;
            await _dBContext.SaveChangesAsync();

            return new Token { AccessToken = AccessToken, RefreshToken = refreshToken};
        }

        public async Task<string> GenerateRefreshToken()
        {
            var secureRandomBytes = new byte[32];

            using var randomNumberGenerator = RandomNumberGenerator.Create();
            await Task.Run(() => randomNumberGenerator.GetBytes(secureRandomBytes));

            var refreshToken = Convert.ToBase64String(secureRandomBytes);
            return refreshToken;
        }
    }
}
