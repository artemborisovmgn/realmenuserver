﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RealMenu.Migrations
{
    public partial class UpdateRev8 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FoodId",
                table: "Reviews");

            migrationBuilder.AddColumn<int>(
                name: "ReviewFoodId",
                table: "Reviews",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Reviews_ReviewFoodId",
                table: "Reviews",
                column: "ReviewFoodId");

            migrationBuilder.AddForeignKey(
                name: "FK_Reviews_Foods_ReviewFoodId",
                table: "Reviews",
                column: "ReviewFoodId",
                principalTable: "Foods",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Reviews_Foods_ReviewFoodId",
                table: "Reviews");

            migrationBuilder.DropIndex(
                name: "IX_Reviews_ReviewFoodId",
                table: "Reviews");

            migrationBuilder.DropColumn(
                name: "ReviewFoodId",
                table: "Reviews");

            migrationBuilder.AddColumn<int>(
                name: "FoodId",
                table: "Reviews",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
