﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RealMenu.Migrations
{
    public partial class UpdateRev6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RestaurantId",
                table: "Reviews",
                newName: "FoodId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "FoodId",
                table: "Reviews",
                newName: "RestaurantId");
        }
    }
}
