﻿using RealMenu.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealMenu.Interfaces
{
    public interface IJwtAuthenticationManager
    {
        Task<Token> AuthenticateAsync(string userName, string password);
    }
}
