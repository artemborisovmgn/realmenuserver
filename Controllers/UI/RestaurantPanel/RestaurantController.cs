﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RealMenu.Data;
using System.Linq;
using System.Threading.Tasks;
using RealMenu.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace RealMenu.Controllers.UI.RestaurantPanel
{
    public class RestaurantController : Controller
    {
        private ApiDBContext _dBContext;

        public RestaurantController(ApiDBContext dBContext)
        {
            _dBContext = dBContext;
        }

        [HttpGet]
        public async Task<IActionResult> GetRestaurantData(int id)
        {
            var authRestaurant = await _dBContext.Restaurants.Where(e => e.Id == id).FirstOrDefaultAsync();
            
            if (ReferenceEquals(authRestaurant, null))
                return NotFound("Restaurant not found");

            RestaurantView restaurantView = new RestaurantView();
            var categories = await _dBContext.Categories.Where(e => e.RestaurantId == id && e.Foods.Count>0).Include(e => e.Foods).ToListAsync();

            authRestaurant.Categories = categories;
            restaurantView.restaurant = authRestaurant;
            var Advertisements = await _dBContext.Advertisements.Where(e => e.RestaurantId == id).ToListAsync();
            var Platforms = await _dBContext.Platforms.Where(e => e.RestaurantId == id).ToListAsync();

            restaurantView.AdvertisementClick = Advertisements.Count;
            restaurantView.IOSViewCount = Platforms.Where(e => e.PlatformType.Equals("IOS")).Count();
            restaurantView.AndroidViewCount = Platforms.Where(e => e.PlatformType.Equals("Android")).Count();

            return View(restaurantView);
        }


    }
}
