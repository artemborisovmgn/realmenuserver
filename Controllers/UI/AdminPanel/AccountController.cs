﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RealMenu.Data;
using RealMenu.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace RealMenu.Controllers
{
    public class AccountController : Controller
    {
        private ApiDBContext _dBContext;

        public AccountController(ApiDBContext dBContext)
        {
            _dBContext = dBContext;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _dBContext.Users.Where(e => e.Email == model.Email && model.Password == e.Password).Include(e=>e.Restaurants).FirstOrDefaultAsync();
                if (!ReferenceEquals(user, null))
                {
                    await Authenticate(model.Email);

                    var restaurantId = user.Restaurants.ToList()[0].Id;
                    //return RedirectToAction("FoodList", "FoodEditor");
                    return RedirectToAction("GetRestaurantData", "Restaurant", new { @id = restaurantId });
                }
                ModelState.AddModelError("LoginError", "User Not found");
            }

            return View(model);
        }

        private async Task Authenticate(string userName)
        {
            // создаем один claim
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, userName)
            };

            ClaimsIdentity id = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            
            // установка аутентификационных куки
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id), new AuthenticationProperties
            {
                IsPersistent = true
            });
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Account");
        }
    }
}