﻿using System;
using Microsoft.AspNetCore.Mvc;
using RealMenu.Data;
using RealMenu.ViewModels;

namespace RealMenu.Controllers
{
    public class FoodEditorController : Controller
    {
        private ApiDBContext _dBContext;

        public FoodEditorController(ApiDBContext dBContext)
        {
            _dBContext = dBContext;
        }

        public ViewResult FoodList() 
        {
            FoodListViewModel foodListViewModel = new FoodListViewModel();
            foodListViewModel.getAllFoodModel = _dBContext.Foods;
            return View(foodListViewModel);
        }
    }
}
