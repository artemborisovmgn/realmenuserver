﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RealMenu.Data;
using RealMenu.ViewModels;

namespace RealMenu.Controllers
{
    public class FoodDetailsController : Controller
    {
        private ApiDBContext _dBContext;

        public FoodDetailsController(ApiDBContext dBContext)
        {
            _dBContext = dBContext;
        }

        [HttpGet]
        public IActionResult FoodInfo()
        {
            return View();
        }

        [HttpPost]
        public async Task<ViewResult> FoodInfo(int Id) 
        {
            FoodDetail foodDetail = new FoodDetail();
            var food = await _dBContext.Foods.FirstOrDefaultAsync(e => e.Id == Id);

            var foodViewCount = await _dBContext.FoodViews.Where(e => e.FoodId == Id).FirstOrDefaultAsync();
            foodDetail.Id = food.Id;
            foodDetail.ImageUrl = food.ImageUrl;
            foodDetail.Ingredients = food.Ingredients;
            foodDetail.MaxSize = food.MaxSize;
            foodDetail.MinSize = food.MinSize;
            foodDetail.Name = food.Name;
            foodDetail.Price = food.Price;
            foodDetail.Spicy = food.Spicy == 1 ? "true" : "false";
            foodDetail.Vegetarian = food.Vegetarian == 1 ? "true" : "false";
            foodDetail.Weight = food.Weight;
            foodDetail.Allergens = food.Allergens == 1 ? "true" : "false";
            foodDetail.Description = foodDetail.Description;
            foodDetail.FoodViewCount = ReferenceEquals(foodViewCount,null) ? 0 : foodViewCount.ViewCount;

            return View(foodDetail);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateFood(FoodDetail foodDetail) 
        {
            var food = await _dBContext.Foods.Where(e => e.Id == foodDetail.Id).FirstOrDefaultAsync();
            var Category = await _dBContext.Categories.Where(e => e.Id == food.CategoryId).FirstOrDefaultAsync();
            
            food.ImageUrl = foodDetail.ImageUrl;
            food.Ingredients = foodDetail.Ingredients;
            food.MaxSize = foodDetail.MaxSize;
            food.MinSize = foodDetail.MinSize;
            food.Name = foodDetail.Name;
            food.Price = foodDetail.Price;
            food.Spicy = !string.IsNullOrEmpty(foodDetail.Spicy) ? (byte)1 : (byte)0; ;
            food.Vegetarian = !string.IsNullOrEmpty(foodDetail.Vegetarian) ? (byte)1 : (byte)0;
            food.Weight = foodDetail.Weight;
            food.Allergens = !string.IsNullOrEmpty(foodDetail.Allergens) ? (byte)1 : (byte)0;
            food.Description = foodDetail.Description;
            await _dBContext.SaveChangesAsync(); 
            return RedirectToAction("GetRestaurantData", "Restaurant", new { @id = Category.RestaurantId });
        }
    }
}
