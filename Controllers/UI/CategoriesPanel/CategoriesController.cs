﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RealMenu.Data;
using RealMenu.ViewModels;
using System.Linq;
using System.Threading.Tasks;

namespace RealMenu.Controllers.UI.CategoriesPanel
{
    public class CategoriesController : Controller
    {
        private ApiDBContext _dBContext;

        public CategoriesController(ApiDBContext dBContext)
        {
            _dBContext = dBContext;
        }

        public async Task<IActionResult> GetFoodsByCategory(int categoryId) 
        {
            CategoryView categoryView = new CategoryView();

            var category = await _dBContext.Categories.Where(e => e.Id == categoryId).FirstOrDefaultAsync();
            var foods = await _dBContext.Foods.Where(e => e.CategoryId == categoryId).ToListAsync();
            var categoryViewCount = await _dBContext.CategoryViews.Where(e => e.CategoryID == categoryId).FirstOrDefaultAsync();

            categoryView.CategoryViewCount = categoryViewCount.ViewCount;
            categoryView.CategoryName = category.Name;
            categoryView.categoryID = categoryId;
            categoryView.Foods = foods;

            return Ok(categoryView);
        }
    }
}
