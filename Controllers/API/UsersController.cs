﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RealMenu.Data;
using RealMenu.Models;
using System.Linq;
using System.Threading.Tasks;

namespace RealMenu.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private ApiDBContext _dbContext;

        public UsersController(ApiDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpPost]
        public async Task<IActionResult> UpdateUser([FromBody]User user)
        {
            var dbUser = await _dbContext.Users.Where(e => e.Id == user.Id).FirstOrDefaultAsync();
            
            if (ReferenceEquals(dbUser, null))
                return BadRequest("User not found");

            dbUser.Name = user.Name;
            dbUser.ImageUrl = user.ImageUrl;
            return Ok();
        }
    }
}
