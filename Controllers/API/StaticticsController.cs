﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RealMenu.Data;
using RealMenu.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealMenu.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class StaticticsController : ControllerBase
    {
        private ApiDBContext _dbContext;

        public StaticticsController(ApiDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetFoodViews(int FoodId)
        {
            var foodViews = await _dbContext.FoodViews.Where(e => e.FoodId == FoodId).ToListAsync();

            if (foodViews.Count == 0)
                return NoContent();

            return Ok(foodViews);   
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetCategoryViews(int CategoryId)
        {
            var categoryViews = await _dbContext.CategoryViews.Where(e => e.CategoryID == CategoryId).ToListAsync();

            if (categoryViews.Count == 0)
                return NoContent();

            return Ok(categoryViews);
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetAdvertisementsClick(int RestaurantId)
        {
            var AdvertisementsClicks = await _dbContext.Advertisements.Where(e => e.RestaurantId == RestaurantId).ToListAsync();

            if (AdvertisementsClicks.Count == 0)
                return NoContent();

            return Ok(AdvertisementsClicks);
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetUsersPlatform(int RestaurantId)
        {
            var platforms = await _dbContext.Platforms.Where(e => e.RestaurantId == RestaurantId).ToListAsync();

            if (platforms.Count == 0)
                return NoContent();

            return Ok(platforms);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> UpdateOrCreateFoodViews(int FoodId)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var dbFoodView = await _dbContext.FoodViews.Where(e => e.FoodId == FoodId).FirstOrDefaultAsync();

            if (!ReferenceEquals(dbFoodView, null))
            {
                dbFoodView.ViewCount += 1;
            }
            else 
            {
                FoodView foodView = new FoodView();
                foodView.FoodId = FoodId;
                foodView.ViewCount = 1;
                
                await _dbContext.FoodViews.AddAsync(foodView);
            }

            await _dbContext.SaveChangesAsync();

            return Ok("FoodViewUpdate update");
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> UpdateOrCreateCategoryViews(int CategoryId)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var dbCategoryView = await _dbContext.CategoryViews.Where(e => e.CategoryID == CategoryId).FirstOrDefaultAsync();

            if (!ReferenceEquals(dbCategoryView, null))
            {
                dbCategoryView.ViewCount += 1;
            }
            else
            {
                CategoryView CategoryView = new CategoryView();
                CategoryView.CategoryID = CategoryId;
                CategoryView.ViewCount = 1;

                await _dbContext.CategoryViews.AddAsync(CategoryView);
            }

            await _dbContext.SaveChangesAsync();

            return Ok("CategoryViewUpdate update");
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> CreateAdvertisementClick([FromBody] Advertisement advertisement)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            await _dbContext.Advertisements.AddAsync(advertisement);
            await _dbContext.SaveChangesAsync();

            return Ok("FoodViewUpdate update");
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> CreatePlatformStatistics([FromBody] Platform platform)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            await _dbContext.Platforms.AddAsync(platform);
            await _dbContext.SaveChangesAsync();

            return Ok("FoodViewUpdate update");
        }
    }
}
