﻿using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RealMenu.Data;
using RealMenu.Helpers;
using RealMenu.Models;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace RealMenu.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class FoodsController : ControllerBase
    {
        private ApiDBContext _dBContext;

        public FoodsController(ApiDBContext dBContext)
        {
            _dBContext = dBContext;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromForm] Food foodObj)
        {
            var imageUrl = await FileHelper.UploadImage(foodObj.Image);
            foodObj.ImageUrl = imageUrl;
            await _dBContext.Foods.AddAsync(foodObj);
            await _dBContext.SaveChangesAsync();
            return StatusCode(StatusCodes.Status201Created);
        }

        // GET: api/Foods?categoryID=
        [HttpGet("{categoryID}")]
        public async Task<IActionResult> Foods(int categoryID)
        {
            var food = await _dBContext.Foods.Where(e=>e.CategoryId == categoryID).ToListAsync();

            if (ReferenceEquals(food, null))
            {
                return NotFound("No Record found against this category Id");
            }
            return Ok(food);
        }
    }
}
