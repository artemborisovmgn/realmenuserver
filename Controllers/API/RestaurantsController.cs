﻿using Microsoft.AspNetCore.Mvc;
using RealMenu.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RealMenu.Models;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using RealMenu.Helpers;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace RealMenu.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class RestaurantsController : ControllerBase
    {
        private ApiDBContext _dbContext;

        public RestaurantsController(ApiDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        [AllowAnonymous]
        [HttpGet("[action]")]
        public async Task<IActionResult> Identification(int id) 
        {
            //var authUser = await _dbContext.Users.Where(e=>e.Login == user.Login).Where(e => e.Password == user.Password).FirstAsync();
            var authRestaurant = await _dbContext.Restaurants.Where(e => e.Id == id).FirstOrDefaultAsync();
            
            if(ReferenceEquals(authRestaurant, null))
                return NotFound("Restaurant not found");

            var categories = await _dbContext.Categories.Where(e=>e.RestaurantId == id).Include(e=>e.Foods).ToListAsync();
            
            for (int i = 0; i < categories.Count; i++)
            {
                if (categories[i].parentID == 0)
                {
                    categories[i].childCategories = new List<Category>();
                    categories[i].childCategories = categories.FindAll(e => e.parentID == categories[i].Id);
                }
            }
            authRestaurant.Categories = categories.FindAll((e) => e.parentID == 0);
            return Ok(authRestaurant);
        }


        [HttpPost("[action]")]
        public async Task<IActionResult> UpdateRestaurant([FromBody] Restaurant restaurant)
        {
            var dbRestaurant = await _dbContext.Restaurants.Where(e => e.Id == restaurant.Id).FirstOrDefaultAsync();

            if (ReferenceEquals(dbRestaurant, null))
                return BadRequest("Restaurant not found");

            dbRestaurant.Description = restaurant.Description;
            dbRestaurant.ImageUrl = restaurant.ImageUrl;
            dbRestaurant.Name = restaurant.Name;
            dbRestaurant.Status = restaurant.Status;

            await _dbContext.SaveChangesAsync();

            return Ok("restourant update");
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> UploadImage([FromBody] Image img) 
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var dbRestaurant = await _dbContext.Restaurants.Where(e => e.Id == img.SourceId).FirstOrDefaultAsync();
            if (ReferenceEquals(dbRestaurant, null))
                return BadRequest("Restaurant dont find");

            var stream = new MemoryStream(img.ImageData);
            IFormFile file = new FormFile(stream, 0, img.ImageData.Length, img.Id.ToString(), img.Tittle);
            var imageUrl = await FileHelper.UploadImage(file);
            
            dbRestaurant.ImageUrl = imageUrl;

            await _dbContext.SaveChangesAsync();

            return Ok("Image upload");
        }
    }
}
