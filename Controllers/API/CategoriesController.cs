﻿using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RealMenu.Data;
using RealMenu.Helpers;
using RealMenu.Models;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace RealMenu.Controllers
{
    [Authorize(Roles ="Administrator")]
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private ApiDBContext _dbContext;

        public CategoriesController(ApiDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromForm] Category categoryObj)
        {
            var imageUrl = await FileHelper.UploadImage(categoryObj.Image);
            categoryObj.ImageUrl = imageUrl;
            await _dbContext.Categories.AddAsync(categoryObj);
            await _dbContext.SaveChangesAsync();
            return StatusCode(StatusCodes.Status201Created);
        }

        // GET: api/Categories?restaurantId=
        [HttpGet("{restaurantId}")]
        public async Task<IActionResult> GetCategories(int restaurantId)
        {
            var users = await (from category in _dbContext.Categories
                where category.RestaurantId == restaurantId
                               select new
                {
                    Id = category.Id,
                    Name = category.Name,
                    ImageUrl = category.ImageUrl,
                    ChildCategories = category.childCategories,
                    Foods = category.Foods
                }).ToListAsync();

            return Ok(users);
        }
    }
}
