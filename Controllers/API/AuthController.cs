﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RealMenu.Data;
using RealMenu.Helpers;
using RealMenu.Models;
using RealMenu.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealMenu.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private JwtAuthenticationManager _jwtAuthenticationManager;
        private ApiDBContext _dbContext;

        public AuthController(ApiDBContext dbContext, JwtAuthenticationManager jwtAuthenticationManager) 
        {
            _dbContext = dbContext;
            _jwtAuthenticationManager = jwtAuthenticationManager;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Authentication([FromBody] AuthModel authModel) 
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var token = await _jwtAuthenticationManager.AuthenticateAsync(authModel.Email, authModel.Password);

            return string.IsNullOrEmpty(token.AccessToken) ? Unauthorized("User does not exist") : Ok(token);
        }

        [AllowAnonymous]
        [HttpPost("[action]")]
        public async Task<IActionResult> RefreshToken([FromBody] Token RefreshToken) 
        {
            var user = await _dbContext.Users.Where(e=>e.RefreshToken == RefreshToken.RefreshToken).SingleAsync();

            if(ReferenceEquals(user,null))
                return BadRequest("User Not Found");

            var token = await _jwtAuthenticationManager.AuthenticateAsync(user.Email, user.Password);

            return Ok(token);
        }

        // GET api/Users/UserDetail
        [HttpGet("[action]")]
        public async Task<IActionResult> UserDetail()
        {
            var user = await _dbContext.Users.Where(a => a.Email == User.Identity.Name)
                .Include(a => a.Restaurants).SingleAsync();

            var restaurants = user.Restaurants.ToList();
            for (int i = 0; i < restaurants.Count; i++)
            {
                var restaurant = restaurants[i];
                var categories = await _dbContext.Categories.Where(e => e.RestaurantId == restaurant.Id)
                    .Include(e => e.Foods)
                    .ToListAsync();

                for (int y = 0; y < categories.Count; y++)
                {
                    if (categories[y].parentID == 0)
                    {
                        categories[y].childCategories = new List<Category>();
                        categories[y].childCategories = categories.FindAll(e => e.parentID == categories[y].Id);
                    }
                }
                restaurant.Categories = categories.FindAll((e) => e.parentID == 0);
            }

            if (ReferenceEquals(user, null))
            {
                return NotFound("You not Authorized or token time out ");
            }
            return Ok(user);
        }
    }
}
