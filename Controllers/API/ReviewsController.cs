﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RealMenu.Data;
using RealMenu.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealMenu.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReviewsController : ControllerBase
    {
        private ApiDBContext _dbContext;

        class Reviews 
        {
            public List<Review> reviews { get; set; } = new List<Review>();
        }

        public ReviewsController(ApiDBContext dbContext) 
        {
            _dbContext = dbContext;
        }

        // GET api/<ReviewsController>/5
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAllReviews(int foodId)
        {
            var reviews = await _dbContext.Reviews.Where(e=>e.FoodId== foodId).ToListAsync();

            var reviewList = new Reviews();
            reviewList.reviews = reviews;
            
            if (reviewList.reviews == null || reviewList.reviews.Count == 0)
                return NotFound("Food not found or reviews no writing");

            return Ok(reviewList);
        }

        // POST api/<ReviewsController>
        [HttpPost("[action]")]
        public async Task<IActionResult> Create([FromForm] Review review)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            if (!ReferenceEquals(review.Image, null) ) 
            {
                var fileUrl = await Helpers.FileHelper.UploadImage(review.Image);

                review.ImageUrl = fileUrl;
            }

            await _dbContext.Reviews.AddAsync(review);
            await _dbContext.SaveChangesAsync();

            return StatusCode(StatusCodes.Status201Created);
        }
    }
}
