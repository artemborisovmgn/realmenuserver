﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RealMenu.Data;
using RealMenu.Helpers;
using RealMenu.Models;
using System.Linq;
using System.Threading.Tasks;

namespace RealMenu.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class FoodModelBidsController : ControllerBase
    {
        private ApiDBContext _dbContext;

        public FoodModelBidsController(ApiDBContext dbContext)
        {
            _dbContext = dbContext;

        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Update([FromBody]FoodModelBid modelBid)
        {
            var bid = await _dbContext.FoodModelBids.Where(e=>e.Id == modelBid.Id).FirstOrDefaultAsync();

            if (ReferenceEquals(bid, null))
                return NotFound("ModelBid Not found");

            bid.Status = modelBid.Status;
            bid.Time = System.DateTime.Now;
            bid.Number = bid.Number++;
            
            return StatusCode(StatusCodes.Status200OK);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Create([FromForm]FoodModelBid modelBid) 
        {
            var fileUrl = await FileHelper.UploadFile(modelBid.MediaFile);
            modelBid.MediaFilePath = fileUrl;
            
            modelBid.Time = System.DateTime.Now;

            await _dbContext.FoodModelBids.AddAsync(modelBid);
            await _dbContext.SaveChangesAsync();

            return StatusCode(StatusCodes.Status201Created);
        }

        [HttpGet("[action]/{foodId}")]
        public async Task<IActionResult> GetBidFromFoodId(int foodId)
        {
            var food = await _dbContext.Foods.Where(e => e.Id == foodId).SingleOrDefaultAsync();

            if (ReferenceEquals(food, null))
                return NotFound("Food with this id not found");

            return !ReferenceEquals(food.modelBid, null) ? Ok(food.modelBid) : NotFound("This food does not exist bid ");
        }

        [HttpGet("[action]/{bidId}")]
        public async Task<IActionResult> GetFromBidId(int bidId) 
        {
            var bids = await _dbContext.FoodModelBids.Where(e => e.Id == bidId).FirstOrDefaultAsync();

            return !ReferenceEquals(bids, null) ? Ok(bids) : NotFound("Not foud bid from this id");
        }
    }
}
