﻿using Microsoft.EntityFrameworkCore;
using RealMenu.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealMenu.Data
{
    public class ApiDBContext : DbContext
    {
        public ApiDBContext(DbContextOptions<ApiDBContext>options) : base(options){}

        public DbSet<User> Users { get; set; }
        public DbSet<Food> Foods { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<UserRole> Roles { get; set; }
        public DbSet<FoodModelBid> FoodModelBids { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Advertisement> Advertisements { get; set; }
        public DbSet<Platform> Platforms { get; set; }
        public DbSet<FoodView> FoodViews { get; set; }
        public DbSet<CategoryView> CategoryViews { get; set; }
        public DbSet<Statistics> Staticstics { get; set; }
    }
}
