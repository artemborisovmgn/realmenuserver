﻿using RealMenu.Models;

namespace RealMenu.ViewModels
{
    public class FoodDetail
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { set; get; }
        public string Ingredients { set; get; }
        public string ImageUrl { get; set; }
        public string Allergens { set; get; }
        public float MaxSize { set; get; }
        public float MinSize { set; get; }
        public string Price { set; get; }
        public string Spicy { set; get; }
        public string Vegetarian { set; get; }
        public int Weight { set; get; }
        public int FoodViewCount { set; get; }
    }
}
