﻿using RealMenu.Models;
using System.Collections.Generic;

namespace RealMenu.ViewModels
{
    public class CategoryView
    {
        public int categoryID { set; get; }

        public string CategoryName { set; get; }
        
        public int CategoryViewCount { set; get; }

        public IEnumerable<Food> Foods { get; set; }
    }
}
