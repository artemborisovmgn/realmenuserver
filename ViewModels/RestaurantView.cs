﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RealMenu.Models;

namespace RealMenu.ViewModels
{
    public class RestaurantView
    {
        public User User { get; set; }
        public Restaurant restaurant { get; set; }
        public int AdvertisementClick { get; set; }
        public int IOSViewCount { get; set; }
        public int AndroidViewCount { get; set; }
    }
}
