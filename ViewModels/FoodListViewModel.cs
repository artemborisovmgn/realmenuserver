﻿using RealMenu.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealMenu.ViewModels
{
    public class FoodListViewModel
    {
        public IEnumerable<Food> getAllFoodModel { get; set; }
    }
}
