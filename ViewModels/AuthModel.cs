﻿using System.ComponentModel.DataAnnotations;

namespace RealMenu.ViewModels
{
    public class AuthModel
    {
        [Required(ErrorMessage = "Don't miss email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Don't miss password")]
        public string Password { get; set; }
    }
}
